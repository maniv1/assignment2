from flask import Flask, request, Response, render_template, jsonify, json
import xml.etree.ElementTree as ET
import requests

app = Flask(__name__)


@app.route('/')
def index():
    return jsonify({"About": "Welcome to austin pool information website!"})


@app.route('/pools', methods=['GET'])
def get_pools():
    result = []
    if request.method == 'GET':
        for i in range(len(pool_name1)):
            new = {"pool_name": pool_name1[i]}
            result.append(new)
    else:
        quit()
    return jsonify({'pools': result})

etags = []

@app.route('/pools/<pname>', methods=['GET'])
def pools(pname):

    etag_val = request.headers.get('If-None-Match')

    if etag_val in etags:
        return "", 304


    if pname in pool_name1:
        idx = pool_name1.index(pname)
        to_return = jsonify({"pool_name": pool_name1[idx], "status": status1[idx], "phone": phone1[idx],"pool_type": pool_type1[idx]})
        etags.append(pname)
        return to_return, 200, {'ETag': pool_name1[idx]}
    elif pname not in pool_name1:
        return "{error: " + pname + " not found}", 404


src = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"
data = requests.get(src)
root = ET.fromstring(data.text)
pool_name1, status1, phone1, pool_type1 = [], [], [], []
pool_dict = {}
etag_list = []

for pool in root.findall('row'):
    pool_name = ''
    status = ''
    phone = ''
    pool_type = ''
    try:
        pool_name = pool.find('pool_name').text
        status = pool.find('status').text
        phone = pool.find('phone').text
        pool_type = pool.find('pool_type').text
        pool_name1.append(pool_name), status1.append(status), phone1.append(phone), pool_type1.append(pool_type)
        pool_dict['pool_name', 'status', 'phone', 'pool_type'] = pool_name, status, phone, pool_type
    except AttributeError:
        continue


if __name__ == '__main__':
    app.run()